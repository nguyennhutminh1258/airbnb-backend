const router = require("express").Router();
const favoriteController = require("../controllers/favorite.controller");

router.post("/add", favoriteController.addFavoriteListing);
router.post("/remove", favoriteController.removeFavoriteListing);
router.get("/:userId", favoriteController.getFavoriteListings);

module.exports = router;