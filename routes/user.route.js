const router = require("express").Router();
const userController = require("../controllers/user.controller");

router.post("/initial-user", userController.initialUser);

module.exports = router;