const router = require("express").Router();
const reservationController = require("../controllers/reservation.controller");

router.get("/", reservationController.getReservation);
router.post("/", reservationController.createReservation);
router.delete("/:userId", reservationController.deleteReservation);

module.exports = router;