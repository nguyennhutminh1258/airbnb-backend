const router = require("express").Router();
const listingController = require("../controllers/listing.controller");

router.post("/", listingController.createListing);
router.get("/", listingController.getListing);
router.get("/:listingId", listingController.getListingById);
router.delete("/:userId", listingController.deleteListing);

module.exports = router;