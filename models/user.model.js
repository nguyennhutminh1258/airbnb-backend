const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    email: {
      type: String,
      unique: true,
    },
    userId: {
      type: String,
    },
    imageUrl: {
      type: String,
    },
    favoriteIds: [{ type: mongoose.Types.ObjectId, ref: "listing" }],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("user", userSchema);
