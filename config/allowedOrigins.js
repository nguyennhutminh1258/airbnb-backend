const allowedOrigins = [
  "http://localhost:3000",
  "https://airbnb-xcn5.onrender.com",
];

module.exports = allowedOrigins;
