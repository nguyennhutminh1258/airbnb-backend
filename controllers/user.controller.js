const User = require("../models/user.model");

const userController = {
  initialUser: async (req, res) => {
    try {
      const { id, firstName, lastName, imageUrl, emailAddresses } = req.body;

      const existedUser = await User.findOne({ userId: id });

      if (existedUser) {
        return res.json({ msg: "Người dùng đã tồn tại", user: existedUser });
      }

      const newUser = new User({
        userId: id,
        name: `${firstName} ${lastName}`,
        imageUrl: imageUrl,
        email: emailAddresses[0].emailAddress,
      });

      await newUser.save();

      return res.json({ msg: "Khởi tạo người dùng thành công", user: newUser });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },
};

module.exports = userController;
