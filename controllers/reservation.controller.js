const Reservation = require("../models/reservation.model");
const Listing = require("../models/listing.model");

const reservationController = {
  createReservation: async (req, res) => {
    try {
      const { listingId, startDate, endDate, totalPrice, userId } = req.body;

      if (!listingId || !startDate || !endDate || !totalPrice) {
        return res.status(400).json({
          msg: "Thiếu thông tin để đặt phòng",
        });
      }

      const newReservation = new Reservation({
        userId,
        listingId,
        startDate,
        endDate,
        totalPrice,
      });

      await newReservation.save();

      return res.json({
        msg: "Đặt phòng thành công",
        reservation: newReservation,
      });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },

  getReservation: async (req, res) => {
    try {
      const { listingId, userId, authorId } = req.query;

      if (listingId) {
        const reservation = await Reservation.find({ listingId })
          .populate("listingId")
          .sort("-createdAt");

        return res.json({
          msg: "Lấy danh sách đặt phòng thành công bằng listingId",
          reservation: reservation,
        });
      }

      if (userId) {
        const reservation = await Reservation.find({ userId })
          .populate("listingId")
          .sort("-createdAt");

        return res.json({
          msg: "Lấy danh sách đặt phòng thành công bằng userId",
          reservation: reservation,
        });
      }

      if (authorId) {
        // Tìm đơn đặt phòng có authorId là chủ của căn hộ
        const listing = await Listing.find({ userId: authorId });

        let listingIds = listing.map((listingItem) => listingItem._id);
        const reservation = await Reservation.find({
          listingId: { $in: listingIds },
          userId: { $ne: authorId },
        })
          .populate("listingId")
          .sort("-createdAt");

        return res.json({
          msg: "Lấy danh sách đặt phòng thành công bằng authorId",
          reservation: reservation,
        });
      }
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },

  deleteReservation: async (req, res) => {
    try {
      const { userId } = req.params;
      const { reservationId } = req.query;

      const listing = await Listing.find({ userId: userId });

      let listingIds = listing.map((listingItem) => listingItem._id);

      const reservation = await Reservation.deleteMany({
        _id: reservationId,
        $or: [{ userId: userId }, { listingId: { $in: listingIds } }],
      });

      return res.json({
        msg: "Xóa đặt phòng thành công",
        reservation,
      });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },
};

module.exports = reservationController;
