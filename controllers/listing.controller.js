const Listing = require("../models/listing.model");
const Reservation = require("../models/reservation.model");

const listingController = {
  createListing: async (req, res) => {
    try {
      const {
        title,
        description,
        imageSrc,
        category,
        roomCount,
        bathroomCount,
        guestCount,
        location,
        price,
        userId,
      } = req.body;

      if (
        !title ||
        !description ||
        !imageSrc ||
        !category ||
        !roomCount ||
        !bathroomCount ||
        !guestCount ||
        !location ||
        !price ||
        !userId
      ) {
        return res.status(400).json({
          msg: "Thiếu thông tin để tạo căn hộ",
        });
      }

      const newListing = new Listing({
        title,
        description,
        imageSrc,
        category,
        roomCount,
        bathroomCount,
        guestCount,
        locationValue: location.value,
        price: parseInt(price, 10),
        userId,
      });

      await newListing.save();

      return res.json({
        msg: "Khởi tạo Airbnb thành công",
        listing: newListing,
      });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },

  getListing: async (req, res) => {
    try {
      const {
        userId,
        roomCount,
        guestCount,
        bathroomCount,
        locationValue,
        startDate,
        endDate,
        category,
      } = req.query;

      let query = {};

      if (userId) {
        query.userId = userId;
      }

      if (category) {
        query.category = category;
      }

      if (roomCount) {
        query.roomCount = {
          $gte: +roomCount,
        };
      }

      if (guestCount) {
        query.guestCount = {
          $gte: +guestCount,
        };
      }

      if (bathroomCount) {
        query.bathroomCount = {
          $gte: +bathroomCount,
        };
      }

      if (locationValue) {
        query.locationValue = locationValue;
      }

      if (startDate && endDate) {
        const reservation = await Reservation.find({
          $or: [
            { endDate: { $gte: startDate }, startDate: { $lte: startDate } },
            { startDate: { $lte: endDate }, endDate: { $gte: endDate } },
          ],
        });

        let listingIds = reservation.map(
          (reservationItem) => reservationItem.listingId
        );

        query._id = {
          $nin: listingIds,
        };
      }

      const listing = await Listing.find(query).sort("-createdAt");

      return res.json({
        msg: "Lấy tất cả căn hộ thành công",
        listing: listing,
      });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },

  getListingById: async (req, res) => {
    try {
      const { listingId } = req.params;
      const listing = await Listing.findById(listingId).populate("userId");

      return res.json({
        msg: "Lấy căn hộ bằng Id thành công",
        listing: listing,
      });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },

  deleteListing: async (req, res) => {
    try {
      const { userId } = req.params;
      const { listingId } = req.query;

      const listing = await Listing.deleteMany({
        _id: listingId,
        userId: userId,
      });

      await Reservation.deleteMany({ listingId });

      return res.json({
        msg: "Xóa căn hộ thành công",
        listing: listing,
      });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },
};

module.exports = listingController;
