const User = require("../models/user.model");
const Listing = require("../models/listing.model");

const favoriteController = {
  addFavoriteListing: async (req, res) => {
    try {
      const { currentUser, listingId } = req.body;

      if (!currentUser) {
        return res.status(400).json({
          msg: "Thiếu thông tin user",
        });
      }

      if (!listingId) {
        return res.status(400).json({
          msg: "Thiếu listingId",
        });
      }

      let favoriteIds = [...(currentUser.favoriteIds || [])];

      favoriteIds.push(listingId);

      const user = await User.findOneAndUpdate(
        { _id: currentUser._id },
        {
          favoriteIds,
        }
      );

      return res.json({
        msg: "Thêm căn hộ vào phần yêu thích",
        user: { ...user._doc, favoriteIds },
      });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },

  removeFavoriteListing: async (req, res) => {
    try {
      const { currentUser, listingId } = req.body;

      if (!currentUser) {
        return res.status(400).json({
          msg: "Thiếu thông tin user",
        });
      }

      if (!listingId) {
        return res.status(400).json({
          msg: "Thiếu listingId",
        });
      }

      let favoriteIds = [...(currentUser.favoriteIds || [])];

      favoriteIds = favoriteIds.filter((id) => id !== listingId);

      const user = await User.findOneAndUpdate(
        { _id: currentUser._id },
        {
          favoriteIds,
        }
      );

      return res.json({
        msg: "Gỡ bỏ căn hộ vào phần yêu thích",
        user: { ...user._doc, favoriteIds },
      });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },

  getFavoriteListings: async (req, res) => {
    try {
      const { userId } = req.params;

      const user = await User.findById(userId);

      const favoriteListings = await Listing.find({
        _id: { $in: user.favoriteIds },
      });

      return res.json({
        msg: "Lấy thông tin yêu thích thành công",
        listing: favoriteListings,
      });
    } catch (error) {
      return res.status(500).json({
        msg: error.msg,
      });
    }
  },
};

module.exports = favoriteController;
