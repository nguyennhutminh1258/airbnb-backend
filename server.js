require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dbConnect = require("./config/dbConnect");
const PORT = process.env.PORT || 5000;
const app = express();

const userRouter = require("./routes/user.route");
const listingRouter = require("./routes/listing.route");
const favoriteRouter = require("./routes/favorite.route");
const reservationRouter = require("./routes/reservation.route");

const corsOptions = require("./config/corsOptions");

dbConnect();

app.use(cors(corsOptions));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//Routes
app.use("/api/user", userRouter);
app.use("/api/listing", listingRouter);
app.use("/api/favorite", favoriteRouter);
app.use("/api/reservation", reservationRouter);

app.listen(PORT, () => {
    console.log(`Server is running  at PORT ${PORT}`);
});  